package com.spacein.jemasterdatacore;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JeMasterdataCoreApplication {

	public static void main(String[] args) {
		SpringApplication.run(JeMasterdataCoreApplication.class, args);
	}

}
