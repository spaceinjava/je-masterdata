package com.spacein.jemasterdataclient;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JeMasterdataClientApplication {

	public static void main(String[] args) {
		SpringApplication.run(JeMasterdataClientApplication.class, args);
	}

}
